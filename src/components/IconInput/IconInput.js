import React from "react";
import styled from "styled-components";

import { COLORS } from "../../constants";

import Icon from "../Icon";
import VisuallyHidden from "../VisuallyHidden";

const SIZES = {
  small: {
    "--icon-size": "12px",
    "--font-size": 14 / 16 + "rem",
    "--padding": "4px 4px 4px 20px",
    "--font-weight": 400,
    "--border-bottom": "1px solid black",
  },
  large: {
    "--icon-size": "16px",
    "--font-size": 18 / 16 + "rem",
    "--padding": "4px 4px 4px 24px",
    "--font-weight": 700,
    "--border-bottom": "2px solid black",
  },
};

const IconInput = ({ label, icon, width = 250, size, placeholder }) => {
  const styles = SIZES[size];
  return (
    <Wrapper>
      <VisuallyHidden>{label}</VisuallyHidden>
      <IconWrapper id={icon} size={styles["--icon-size"]} strokeWidth={size === 'small' ? 1 : 2} />
      <Input
        style={styles}
        placeholder={placeholder}
        width={width}
      />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: relative;
  font-weight: var(--font-weight);
  color: ${COLORS.gray700};

  &:hover {
    color: ${COLORS.black};
  }
`;

const Input = styled.input`
  border: none;
  border-bottom: var(--border-bottom);
  outline-color: ${COLORS.primary};
  outline-offset: 2px;
  font-size: var(--font-size);
  padding: var(--padding);
  width: ${(p) => p.width}px;
  color: inherit;
  font-weight: inherit;

  &::placeholder {
    color: ${COLORS.gray500};
  }
`;

const IconWrapper = styled(Icon)`
  position: absolute;
  top: 6px;
  left: 0;
  color: inherit;
  font-weight: inherit;
`;

export default IconInput;
