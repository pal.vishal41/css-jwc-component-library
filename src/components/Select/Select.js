import React from 'react';
import styled from 'styled-components';

import { COLORS } from '../../constants';
import Icon from '../Icon';
import { getDisplayedValue } from './Select.helpers';

const Select = ({ label, value, onChange, children }) => {
  const displayedValue = getDisplayedValue(value, children);

  return (
    <Wrapper>
      <SelectedValue>{displayedValue}</SelectedValue>
      <ChevronDown id="chevron-down" size={24} strokeWidth={2} />
      <SelectInput value={value} onChange={onChange}>
        {children}
      </SelectInput>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: relative;
  width: fit-content;
  padding: 12px 52px 12px 16px;
  background-color: ${COLORS.transparentGray15};
  border-radius: 8px;
  color: ${COLORS.gray700};

  &:hover {
    color: ${COLORS.black};
  }
`;

const SelectInput = styled.select`
  position: absolute;
  top: 0;
  left: 0;
  appearance: none;
  width: 100%;
  height: 100%;
  font-size: 1rem;
  border: none;
  border-radius: 8px;
  color: inherit;
  background-color: transparent;
  padding-left: 12px;
  outline-color: ${COLORS.primary};
`;

const ChevronDown = styled(Icon)`
  position: absolute;
  right: 12px;
  top: 8px;
  bottom: 0;
  color: inherit;
`;

const SelectedValue = styled.span`
  visibility: hidden;
`;

export default Select;
