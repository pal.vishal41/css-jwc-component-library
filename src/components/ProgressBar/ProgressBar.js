/* eslint-disable no-unused-vars */
import React from 'react';
import styled from 'styled-components';

import { COLORS } from '../../constants';
import VisuallyHidden from '../VisuallyHidden';

const SIZES = {
  small: {
    "--height": "8px",
    "--borderRadius": "4px",
    "--padding": "0",
  },
  medium: {
    "--height": "12px",
    "--borderRadius": "4px",
    "--padding": "0",
  },
  large: {
    "--height": "24px",
    "--borderRadius": "8px",
    "--padding": "4px",
  },
}

const ProgressBar = ({ value, size }) => {
  const styles = SIZES[size];
  return <Wrapper role="progressbar" aria-valuenow={value} style={styles}>
    <VisuallyHidden>{value}%</VisuallyHidden>
    <Bar width={value} size={size} />
  </Wrapper>;
};

const Wrapper = styled.div`
  width: 370px;
  height: var(--height);
  background-color: ${COLORS.transparentGray15};
  border-radius: var(--borderRadius);
  padding: var(--padding);
  box-shadow: inset 0px 2px 4px ${COLORS.transparentGray35};
`;

const Bar = styled.div`
  width: ${p => p.width}%;
  height: 100%;
  background-color: ${COLORS.primary};
  border-radius: ${p => p.width > 99.5 ? '4px' : '4px 0 0 4px'};
`;

export default ProgressBar;
